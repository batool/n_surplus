This project contains codes to extract gridded N surplus dataset at different country (NUTS0) , sub-country (NUTS 1, 2) and river basin level. We further provides codes to plot snapshots of the N surplus dataset at NUTS, gridded and NUTS level. The gridded dataset used in these codes are annual long-term reconstructed total N surplus (both agricultural and non-agricultural soils) across Europe at a 5 arcmin spatial resolution for the period 1850 to 2019. The datasets (version 1) are archived at the Zenodo repository and can be accessed here: https://doi.org/10.5281/zenodo.6581441. The dataset consists of 16 N surplus estimates that account for the uncertainties resulting from input data sources and methodological choices in major components of the N surplus.This dataset offers the flexibility of aggregating the N surplus at any spatial scale of relevance to support water and land management strategies.

Code description:

1. NUTS0.R:  This file consists of codes to extract N surplus from grid to NUTS 0 level and to derive mean and standard deviations of 16 N surplus estimates 
2. NUTS1.R:  This file consists of codes to extract N surplus from grid to NUTS 1 level and to derive mean and standard deviations of 16 N surplus estimates
3. NUTS2.R:  This file consists of codes to extract N surplus from grid to NUTS 2 level and to derive mean and standard deviations of 16 N surplus estimates
4. river_basin_level:  This file consists of codes to extract N surplus from grid to river basin level and to derive mean and standard deviations of 16 N surplus estimates  
5. grid_level:  This file consists of codes to derive mean and standard deviations of 16 N surplus estimates  at grid level
6. plots: This files consists of codes to plot N surplus at NUTS, river basins and grid level (we provide example of plotting snapshot)

Further information:

Details/Citation: Long-Term Annual Soil Nitrogen Surplus Across Europe 1850-2019 by M. Batool, F.J.Sarrazin, S. Attinger and R. Kumar.\
Further queries regarding these datasets can be directed to Masooma Batool (masooma.batool@ufz.de) and Rohini Kumar (rohini.kumar@ufz.de).\

