

# import libraries
library(readxl)
library(xlsx)
library(raster)
library(tidyverse)
library(zoo)
library(naturalsort)
library(exactextractr)
library(ggplot)
library(RColorBrewer)
library(ncdf4)
library(rasterVis)
library(data.table)
library(rgdal)
library(sf)
library(viridis)
library(lubridate)    

###########################################################################################################################################################  

# Plot gridded dataset at NUTS level 

###########################################################################################################################################################  

# NUTS 0 level ###
NUTS = "0" 
method = "1"  ## change number for the required method type for the N surplus
Years <- c(1850)  ## Add year's for the required/more snapshots
function_plot_NUTS0 <- function (y) {
  df_N_sur <- read.csv(paste0("N_sur_total_kg_ha_NUTS", NUTS, "_v1.csv")) %>%
    filter(Year == y)
  xlabs = seq(-10, 42, 10)
  ylabs = seq(37, 72, 7)
  
  # Import  shapefile
  country <- readOGR("ne_50m_admin_0_countries.shp",
                     stringsAsFactors=FALSE)
  country_shp <- readOGR("World_Countries.shp",
                         stringsAsFactors=FALSE)
  
  ## Import NUTS shapefile and boundary
  p_NUTS_0 <- readOGR("Project_NUTS_0.shp")
  p_NUTS_0_BN <- readOGR("Project_NUTS_0_BN.shp")
  
  # Import raster file #
  fname <- paste0("N_sur_total_kg_ha_grid_area_1850-2019_method_", method, "_v1.0.nc") 
  r  <- raster(fname , varname = "Nsurplus", band = 1) # change band according to the year (Here, as we provide example of 1850, we used band = 1)
  r_NUTS_0 <- rasterize(p_NUTS_0, r)
  r <- r_NUTS_0
  
  ## Make a dataframe
  df <- r %>%
    as.data.frame(., xy = T) %>%
    select(x,y,NUTS_ID = layer_FID)%>%
    full_join(df_N_sur, by = c("NUTS_ID")) %>%
    #drop_na()%>% 
    ## Make cateogries of dataset
    mutate(plot = cut( N_sur_kg_ha,  unique(c(-0, -100, 0.00000000001, 4, 8, 12,16,20,30,40,60,Inf)), 
                       labels=c( "< 0","0","> 0-4","> 4-8","> 8-12","> 12-16","> 16-20",
                                 "> 20-30","> 30-40", "> 40-60","> 60"),
                       include.lowest = TRUE, right = FALSE)) 
  
  # Plot 
  p <- ggplot() +
    geom_raster(data = df , aes(x=x, y=y, fill=plot)) +
    geom_path(data=  country_shp, aes(x=long, y = lat, group=group),
              color= "black", size= 0.2, alpha = 0.2)  +
    coord_equal() +
    cowplot::theme_cowplot() +
    geom_path(data=  p_NUTS_0_BN , aes(x=long, y = lat, group=group),
              color= "black", size= 0.2) +
    scale_x_continuous(breaks = xlabs, labels = paste0(xlabs,'°W')) +
    scale_y_continuous(breaks = ylabs, labels = paste0(ylabs,'°N'))+
    coord_cartesian(xlim =c(-10, 42), ylim = c(37.7, 72))+
    labs(x= "", y = "", title = "")+
    theme_bw(base_size=13) +
    theme(axis.title.x = element_text(size=10),
          plot.margin = margin(0.5,0.5,0.5,0.5,"cm"),
          axis.title.y = element_text(size=10, angle=90),
          axis.text.x = element_blank(),
          axis.text.y = element_text(color = "black",size=10),
          # Format legend ##
          legend.position = "right",
          legend.key.width = unit(0.5, "cm"),
          legend.key.height  = unit(1.75, "cm"),
          legend.text.align = 0.01,
          legend.background = element_rect(fill = alpha('white', 0.0)),
          legend.title = element_text(color = "black", size = 35),
          legend.text = element_text(color = "black", size = 30),
          # Format panel
          strip.background = element_blank(),
          strip.text = element_blank(),
          panel.background = element_rect(fill = "lightblue2"),
          panel.border = element_rect(colour = "black", fill=NA, size=1),
          panel.grid.major = element_blank(),   # Major grid lines
          panel.grid.minor = element_blank(),   # Minor grid lines
          panel.grid.major.x = element_blank(), # Vertical major grid lines
          panel.grid.major.y = element_blank(), # Horizontal major grid lines
          panel.grid.minor.x = element_blank(), # Vertical minor grid lines
          panel.grid.minor.y = element_blank(),  # Vertical major grid lines
          panel.grid = element_blank(),
          legend.key = element_blank())+ 
    scale_fill_viridis(discrete = T,
                       drop = FALSE, # To present all legend categories even values are not within the category
                       expression("N surplus (kg ha"^-1* yr^-1*")")) + 
    theme(legend.position = "") 
  
  pdf(paste0("N_sur_NUTS_0_kg_ha_" ,y, ".pdf"), width=3.5, height=3.5)
  print(p)
  dev.off()
  
  # Crop extra white spaces
  knitr::plot_crop(paste0("N_sur_NUTS_0_kg_ha_" ,y, ".pdf"))
}
lapply(Years, function_plot_NUTS0)

# NUTS 1 level ###
NUTS = "1"
method = "1"  ## change number for the required method type for the N surplus
Years <- c(1850) ## Add year's for the required/more snapshots
function_plot_NUTS1 <- function (y) {
  df_N_sur <- read.csv(paste0("N_sur_total_kg_ha_NUTS", NUTS, "_v1.csv")) %>%
    filter(Year == y)
  xlabs = seq(-10, 42, 10)
  ylabs = seq(37, 72, 7)
  
  Projected_CRS = "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"
  
  # Import country shapefile
  country <- readOGR("ne_50m_admin_0_countries.shp",
                     stringsAsFactors=FALSE)
  
  NUTS_1 <- readOGR("ref-nuts-2021-01m.shp/NUTS_RG_01M_2021_3035_LEVL_1.shp/NUTS_RG_01M_2021_3035_LEVL_1.shp",
                    stringsAsFactors=FALSE)
  NUTS_1_BN <- readOGR("ref-nuts-2021-01m.shp/NUTS_BN_01M_2021_3035_LEVL_1.shp/NUTS_BN_01M_2021_3035_LEVL_1.shp",
                       stringsAsFactors=FALSE)
  NUTS_0 <- readOGR("ref-nuts-2021-01m.shp/NUTS_RG_01M_2021_3035_LEVL_0.shp/NUTS_RG_01M_2021_3035_LEVL_0.shp",
                    stringsAsFactors=FALSE)
  NUTS_0_BN <- readOGR("ref-nuts-2021-01m.shp/NUTS_BN_01M_2021_3035_LEVL_0.shp/NUTS_BN_01M_2021_3035_LEVL_0.shp",
                       stringsAsFactors=FALSE)
  
  p_NUTS_0  <- spTransform(NUTS_0, CRS = Projected_CRS)
  p_NUTS_0_BN  <- spTransform(NUTS_0_BN, CRS = Projected_CRS)
  p_NUTS_1  <- spTransform(NUTS_1, CRS = Projected_CRS)
  p_NUTS_1_BN  <- spTransform(NUTS_1_BN, CRS = Projected_CRS)
  
  # Import NetCDF file #
  fname <- paste0("N_sur_total_kg_ha_grid_area_1850-2019_method_", method, "_v1.0.nc") 
  r  <- raster(fname , varname = "Nsurplus", band = 1) # change band according to the year (Here, as we provide example of 1850, we used band = 1)
  r_NUTS_0 <- rasterize(p_NUTS_0, r)
  r <- r_NUTS_0
 
  df <- r %>%
    as.data.frame(., xy = T) %>%
    select(x,y,NUTS_ID = layer_FID)%>%
    full_join(df_N_sur, by = c("NUTS_ID")) %>%
    mutate(plot = cut( N_sur_kg_ha,  unique(c(-0, -13, 0.00000000001, 4, 8, 12,16,20,30,40,60,Inf)), 
                       labels=c( "0","0","> 0-4","> 4-8","> 8-12","> 12-16","> 16-20",
                                 "> 20-30","> 30-40", "> 40-60","> 60"),
                       include.lowest = TRUE, right = FALSE)) 
  p <- ggplot() +
    geom_raster(data = df , aes(x=x, y=y, fill=plot))   +
    geom_path(data=  country, aes(x=long, y = lat, group=group),
              color= "black", size= 0.1, alpha = 0.2) +
    coord_equal() +
    cowplot::theme_cowplot() +
    geom_path(data=  p_NUTS_0_BN , aes(x=long, y = lat, group=group),
              color= "black", size= 0.2) +
    geom_path(data=  p_NUTS_1_BN , aes(x=long, y = lat, group=group),
              color= "black", size= 0.1) +
    scale_x_continuous(breaks = xlabs, labels = paste0(xlabs,'°W')) +
    scale_y_continuous(breaks = ylabs, labels = paste0(ylabs,'°N'))+
    coord_cartesian(xlim =c(-10, 42), ylim = c(37.7, 72))+
    labs(x= "", y = "", title = "")+
    theme_bw(base_size=13) +
    theme(axis.title.x = element_text(size=10),
          plot.margin = margin(0.5,0.5,0.5,0.5,"cm"),
          axis.title.y = element_text(size=10, angle=90),
          axis.text.x = element_blank(),
          axis.text.y = element_blank(),
          # Format legend ##
          legend.position = "right",
          legend.key.width = unit(0.5, "cm"),
          legend.key.height  = unit(1.75, "cm"),
          legend.text.align = 0.01,
          legend.background = element_rect(fill = alpha('white', 0.0)),
          legend.title = element_text(color = "black", size = 35),
          legend.text = element_text(color = "black", size = 30),
          # Format panel
          strip.background = element_blank(),
          strip.text = element_blank(),
          panel.background = element_rect(fill = "lightblue2"),
          panel.border = element_rect(colour = "black", fill=NA, size=1),
          panel.grid.major = element_blank(),   # Major grid lines
          panel.grid.minor = element_blank(),   # Minor grid lines
          panel.grid.major.x = element_blank(), # Vertical major grid lines
          panel.grid.major.y = element_blank(), # Horizontal major grid lines
          panel.grid.minor.x = element_blank(), # Vertical minor grid lines
          panel.grid.minor.y = element_blank(),  # Vertical major grid lines
          panel.grid = element_blank(),
          legend.key = element_blank())+ 
    scale_fill_viridis(discrete = T,
                       drop = FALSE, # To present all legend categories even values are not within the category
                       expression("N surplus (kg ha"^-1* yr^-1*")")) + 
    theme(legend.position = "") 
  
  pdf(paste0("N_sur_NUTS_1_" ,y, ".pdf"), width=3.5, height=3.5)
  print(p)
  dev.off()
  
  knitr::plot_crop(paste0("N_sur_NUTS_1_" ,y, ".pdf"))
  
}
lapply(Years, function_plot_NUTS1)

# NUTS 2 level ###
NUTS = "2"
method = "1"  ## change number for the required method type for the N surplus
Years <- c(1850) ## Add year's for the required/more snapshots
NUTS_function_plot <- function (y) {
  
  df_N_sur <- read.csv(paste0("N_sur_total_kg_ha_NUTS", NUTS, "_v1.csv")) %>%
    filter(Year == y)
  xlabs = seq(-10, 42, 10)
  ylabs = seq(37, 72, 7)
  
  # Import country shapefile
  country <- readOGR("This_study/crop_production/Inputs/ne_50m_admin_0_countries/ne_50m_admin_0_countries.shp",
                     stringsAsFactors=FALSE)
  
  
  Projected_CRS = "+proj=longlat +datum=WGS84 +no_defs +ellps=WGS84 +towgs84=0,0,0"
  
  
  NUTS_0 <- readOGR("ref-nuts-2021-01m.shp/NUTS_RG_01M_2021_3035_LEVL_0.shp/NUTS_RG_01M_2021_3035_LEVL_0.shp",
                    stringsAsFactors=FALSE)
  NUTS_0_BN <- readOGR("ref-nuts-2021-01m.shp/NUTS_BN_01M_2021_3035_LEVL_0.shp/NUTS_BN_01M_2021_3035_LEVL_0.shp",
                       stringsAsFactors=FALSE)
  NUTS_1 <- readOGR("ref-nuts-2021-01m.shp/NUTS_RG_01M_2021_3035_LEVL_1.shp/NUTS_RG_01M_2021_3035_LEVL_1.shp",
                    stringsAsFactors=FALSE)
  NUTS_1_BN <- readOGR("ref-nuts-2021-01m.shp/NUTS_BN_01M_2021_3035_LEVL_1.shp/NUTS_BN_01M_2021_3035_LEVL_1.shp",
                       stringsAsFactors=FALSE)
  NUTS_2 <- readOGR("ref-nuts-2021-01m.shp/NUTS_RG_01M_2021_3035_LEVL_2.shp/NUTS_RG_01M_2021_3035_LEVL_2.shp",
                    stringsAsFactors=FALSE)
  NUTS_2_BN <- readOGR("ref-nuts-2021-01m.shp/NUTS_BN_01M_2021_3035_LEVL_2.shp/NUTS_BN_01M_2021_3035_LEVL_2.shp",
                       stringsAsFactors=FALSE)
  
  
  p_NUTS_0  <- spTransform(NUTS_0, CRS = Projected_CRS)
  p_NUTS_0_BN  <- spTransform(NUTS_0_BN, CRS = Projected_CRS)
  p_NUTS_1  <- spTransform(NUTS_1, CRS = Projected_CRS)
  p_NUTS_1_BN  <- spTransform(NUTS_1_BN, CRS = Projected_CRS)
  p_NUTS_2  <- spTransform(NUTS_2, CRS = Projected_CRS)
  p_NUTS_2_BN  <- spTransform(NUTS_2_BN, CRS = Projected_CRS)
 
  
  # Import NetCDF file #
  fname <- paste0("N_sur_total_kg_ha_grid_area_1850-2019_method_", method, "_v1.0.nc") 
  r  <- raster(fname , varname = "Nsurplus", band = 1) # change band according to the year (Here, as we provide example of 1850, we used band = 1)
  r_NUTS_0 <- rasterize(p_NUTS_0, r)
  r <- r_NUTS_0
  
  df <- r %>%
    as.data.frame(., xy = T) %>%
    select(x,y,NUTS_ID = layer_FID)%>%
    full_join(df_N_sur, by = c("NUTS_ID")) %>%
    mutate(plot = cut( N_sur_kg_ha,  unique(c(-0, -100, 0.00000000001, 4, 8, 12,16,20,30,40,60,Inf)), 
                       labels=c( "< 0","0","> 0-4","> 4-8","> 8-12","> 12-16","> 16-20",
                                 "> 20-30","> 30-40", "> 40-60","> 60"),
                       include.lowest = TRUE, right = FALSE)) 
  p <- ggplot() +
    geom_raster(data = df , aes(x=x, y=y, fill=plot))   +
    geom_path(data=  country, aes(x=long, y = lat, group=group),
              color= "black", size= 0.1, alpha = 0.2) +
    #theme_bw() +
    coord_equal() +
    cowplot::theme_cowplot() +
    geom_path(data=  p_NUTS_0_BN , aes(x=long, y = lat, group=group),
              color= "black", size= 0.2) +
    geom_path(data=  p_NUTS_1_BN , aes(x=long, y = lat, group=group),
              color= "black", size= 0.1) +
    geom_path(data=  p_NUTS_2_BN , aes(x=long, y = lat, group=group),
              color= "black", size= 0.06) +
    scale_x_continuous(breaks = xlabs, labels = paste0(xlabs,'°W')) +
    scale_y_continuous(breaks = ylabs, labels = paste0(ylabs,'°N'))+
    coord_cartesian(xlim =c(-10, 42), ylim = c(37.7, 72))+
    labs(x= "", y = "", title = "")+
    theme_bw(base_size=13) +
    #coord_sf(xlim = c(-12, 42), ylim = c(36, 72), expand=F)   + 
    theme(axis.title.x = element_text(size=10),
          plot.margin = margin(0.5,0.5,0.5,0.5,"cm"),
          axis.title.y = element_text(size=10, angle=90),
          axis.text.x = element_blank(),
          axis.text.y = element_blank(),
          # Format legend ##
          legend.position = "right",
          legend.key.width = unit(0.5, "cm"),
          legend.key.height  = unit(1.75, "cm"),
          legend.text.align = 0.01,
          legend.background = element_rect(fill = alpha('white', 0.0)),
          legend.title = element_text(color = "black", size = 35),
          legend.text = element_text(color = "black", size = 30),
          #legend.spacing.x = unit(0.01, 'cm'),
          # Format panel
          strip.background = element_blank(),
          #strip.text = element_text(colour = 'black'),
          strip.text = element_blank(),
          panel.background = element_rect(fill = "lightblue2"),
          panel.border = element_rect(colour = "black", fill=NA, size=1),
          panel.grid.major = element_blank(),   # Major grid lines
          panel.grid.minor = element_blank(),   # Minor grid lines
          panel.grid.major.x = element_blank(), # Vertical major grid lines
          panel.grid.major.y = element_blank(), # Horizontal major grid lines
          panel.grid.minor.x = element_blank(), # Vertical minor grid lines
          panel.grid.minor.y = element_blank(),  # Vertical major grid lines
          panel.grid = element_blank(),
          legend.key = element_blank())+ 
    scale_fill_viridis(discrete = T,
                       drop = FALSE, # To present all legend categories even values are not within the category
                       expression("N surplus (kg ha"^-1* yr^-1*")")) + 
    theme(legend.position = "") 
  
  pdf(paste0("N_sur_NUTS_2_" ,y, ".pdf"), width=3.5, height=3.5)
  print(p)
  dev.off()
  
  knitr::plot_crop(paste0("N_sur_NUTS_2_" ,y, ".pdf"))
  
}
lapply(Years, NUTS_function_plot)

###########################################################################################################################################################  

# Plot gridded dataset 

###########################################################################################################################################################  

Years <- c(1850) ## Add year's for the required/more snapshots
method = "1"  ## change number for the required method type for the N surplus
plot_N_sur <- function(y) {
  
  xlabs = seq(-10, 42, 10)
  ylabs = seq(37, 72, 7)
  title =  paste0(y)

  outpath <- "Plots/"
  y_title = "N surplus"
  name = "Nsur_total_kg_ha"
  
  # Import country shapefile
  country <- readOGR("ne_50m_admin_0_countries/ne_50m_admin_0_countries.shp",
                     stringsAsFactors=FALSE)
  EU_garea <- raster("Eu_garea_hyde_ha.tif")
  
  # Import NetCDF file for the N surplus
  fname <- paste0("N_sur_total_kg_ha_grid_area_1850-2019_method_", method, "_v1.0.nc") 
  r  <- raster(fname , varname = "Nsurplus", band = 1)
  r[is.na(r)] <- 0
  r_mask <- mask(r, country)
  
  df <- r_mask %>%
    as.data.frame(., xy=T) %>%
    drop_na()%>%
    rename(Value = layer) %>%
    mutate(plot = cut(Value,  unique(c(-0, -7000, 0.00000000001, 8, 12, 16 ,20, 30,45,50,60,Inf)), 
                      labels=c( "","0","8","12","16","20","30","45","50","60","80"),
                      include.lowest = TRUE, right = FALSE)) 
  
  ggplot() +
    geom_raster(data = df , aes(x=x, y=y, fill= plot)) +
    labs(x= "", y = "", title = "")+
    coord_equal() +
    cowplot::theme_cowplot() +
    geom_polygon(data= country, aes(x=long, y = lat, group=group),
                 fill = NA, color= "black", size= 0.75) +
    scale_x_continuous(breaks = xlabs, labels = paste0(xlabs,'°W')) +
    scale_y_continuous(breaks = ylabs, labels = paste0(ylabs,'°N'))+
    coord_cartesian(xlim =c(-10, 42), ylim = c(37.7, 72))+
    theme_bw(base_size=13) +
    theme(plot.margin = margin(0.5,0.5,0.5,0.5,"cm"),
          axis.title.x = element_text(size=30),
          axis.title.y = element_text(size=30, angle=90),
          axis.text.x = element_blank(),
          axis.text.y = element_text(color = "black",size=45),
          # Format legend ##
          legend.key.width = unit(4, "cm"),
          legend.key.height  = unit(1.1, "cm"),
          legend.text.align = 0.01,
          legend.background = element_rect(fill = alpha('white', 0.0)),
          legend.title = element_text(color = "black", size = 40),
          legend.text = element_text(color = "black", size = 40),
          legend.spacing.x = unit(0, 'cm'),
          panel.background = element_rect(fill = "lightblue"),
          panel.border = element_rect(colour = "black", fill=NA, size=1),
          panel.grid.major = element_blank(),   # Major grid lines
          panel.grid.minor = element_blank(),   # Minor grid lines
          panel.grid.major.x = element_blank(), # Vertical major grid lines
          panel.grid.major.y = element_blank(), # Horizontal major grid lines
          panel.grid.minor.x = element_blank(), # Vertical minor grid lines
          panel.grid.minor.y = element_blank(),  # Vertical major grid lines
          panel.grid = element_blank(),
          legend.key = element_blank())+
    scale_fill_viridis(discrete = T,
                       expression("N surplus (kg ha"^-1*")"),
                       guide = guide_legend(override.aes = list(size = 3,alpha = 1))) + 
    theme(legend.position = "none") #+
  guides(fill = guide_legend(nrow=1,override.aes = list(alpha = 1), 
                             label.position="bottom",
                             label.hjust = -0.01)) 

  ggsave(paste0(outpath, name, "_" ,y, ".pdf"),
         device = "pdf", width = 21, height = 14, dpi = 300)
  
  knitr::plot_crop(paste0(outpath, name, "_" ,y, ".pdf"))
  
}
lapply(Years, plot_N_sur)

###########################################################################################################################################################  

# Plot gridded dataset for N surplus for river basins

###########################################################################################################################################################  

# write function to plot the N surplus at river basin level 
function_country_plot <- function(df) {
  
  ## filter out rivers for the plot
  df_plot <- df %>%
    select(RIVER_BASI,Year, value = value)  %>%
    filter(RIVER_BASI == "DANUBE" | RIVER_BASI == "ELBE" | RIVER_BASI == "RHINE"
           | RIVER_BASI == "PO" | RIVER_BASI == "SEINE" | RIVER_BASI == "TAGUS" 
           | RIVER_BASI == "EBRO" | RIVER_BASI == "GUADIANA" | RIVER_BASI == "EBRO" 
           | RIVER_BASI == "THAMES" | RIVER_BASI == "WESER" 
           | RIVER_BASI == "ODER") %>%
    mutate(RIVER_BASI = as.character(RIVER_BASI))%>%
    mutate(RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "DANUBE", "Danube"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "ELBE", "Elbe"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "RHINE", "Rhine"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "PO", "Po"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "SEINE", "Seine"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "TAGUS", "Tagus"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "EBRO", "Ebro"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "GUADIANA", "Guadiana"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "THAMES", "Thames"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "WESER", "Weser"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "ODER", "Oder"),
           RIVER_BASI = replace(RIVER_BASI, RIVER_BASI == "EBRO", "Ebro")) %>%
    group_by(RIVER_BASI)%>%
    mutate(ma = SMA(value, n = 5)) ### Calculate 5 years moving average
  
  
  ###### Plot ####
  
  ggplot()+
    geom_line(data = df_plot, aes(x= Year, y = ma, group = RIVER_BASI, color = RIVER_BASI, linetype= RIVER_BASI), size = 1.25) +
    labs(x= "", y  = "", title = "") + # Insert labels
    theme(axis.text.x = element_text(colour = "black", size = 35, angle = 90, hjust = 0.5, vjust = 0.5),
          panel.grid.major = element_line(colour="gray50", size=0.2, linetype = 2),
          axis.text.y = element_text(colour = "black", size = 35),
          plot.title = element_text(margin = margin(10, 0, 10, 0), size = 35),
          strip.text = element_text( hjust = 0.15, size = 25),
          axis.title.y = element_text(size = 35),
          axis.title.x = element_text(size = 35),
          strip.background = element_rect(fill=NA,linetype= "solid"),
          panel.background = element_blank(),
          panel.border = element_rect(colour = "black", fill=NA, size=1),
          legend.position = c(0.005, 1), ## Legend position at top left of the panel
          legend.justification = c(0, 1), 
          legend.text = element_text(size = 25),
          legend.background = element_rect(fill = NA), ## Keeps legend within the panel boarder 
          legend.title= element_blank(),
          legend.key=element_blank()) +
    scale_color_brewer(palette="Paired")+
    scale_x_continuous(breaks = scales::pretty_breaks(n = 7) , expand = c(0.001, 0.001)) +
    scale_y_continuous(breaks = scales::pretty_breaks(n = 7)) +
    guides(linetype = guide_legend(override.aes = list(size = 3))) +
    theme(legend.key.size = grid::unit(2.5, "lines")) +
    scale_linetype_manual(values=c("dotted", "dotdash","twodash", "twodash","dotdash", "dotdash","twodash", "dotdash",
                                   "dotdash", "twodash", "dotdash"))+
    scale_color_manual(values=c("#00AFBB","Olivedrab3", "darkgreen", "red", "blue", "purple", "Orange",
                                "palegreen4", "cyan1", "gold4", "lightpink"))
  ggsave(paste0(outpath, ".pdf"),
         device = "pdf", width=27*1.25, height=20*1.25, units="cm", dpi=300)
}
######### Total N surplus #######
df <- read.csv("N_sur_total_kg_ha_river_basin_v1.csv")
value = "N_sur_total_kg_ha_river_basin"
y = expression('N surplus ' * '(kg' * ' ha' ^-1 *' yr' ^-1 *')')
outpath <- paste0("N_sur_total_kg_ha_river_basin")
plot <- function_country_plot (df)

knitr::plot_crop("N_sur_total_kg_ha_river_basin.pdf")



